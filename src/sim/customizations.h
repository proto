/* User plug-in point for simulator customizations
Copyright (C) 2005-2008, Jonathan Bachrach, Jacob Beal, and contributors 
listed in the AUTHORS file in the MIT Proto distribution's top directory.

This file is part of MIT Proto, and is distributed under the terms of
the GNU General Public License, with a linking exception, as described
in the file LICENSE in the MIT Proto distribution's top directory. */

// This file is where declarations for simulator customizations
// can be put.  By default, it is empty.

#ifndef __CUSTOMIZATIONS__
#define __CUSTOMIZATIONS__

#endif // __CUSTOMIZATIONS__
