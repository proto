AC_DEFUN([PROTO_WITH_ODE], [
    AC_ARG_WITH([ode],
                [AS_HELP_STRING([--with-ode[=DIR]],
                   [use Open Dynamics Engine, optionally in DIR/lib and DIR/include])],
                [use_ode=$withval],
                [use_ode=meh])

    if test x"$use_ode" != x"no"; then

        # if the user gave a path, append it to LDFLAGS
        case "$use_ode" in
            */*)
                LDFLAGS="$LDFLAGS -L$use_ode/lib"
                CPPFLAGS="$CPPFLAGS -I$use_ode/include"
                use_ode=yes
                ;;
        esac

        AC_CHECK_LIB([ode], [dBodyCreate],
            [
                have_ode=yes
                LIBS="-lode $LIBS"
            ], [
                have_ode=no
            ])

        if test x"$have_ode" = x"yes"; then
            WANT_ODE=true
        else
            case $use_ode in
                yes)
                    AC_MSG_ERROR([Cannot find libode.])
                    ;;
                meh)
                    AC_MSG_WARN([Cannot find libode; will build without it])
                    ;;
            esac
            WANT_ODE=false
        fi
    else
        WANT_ODE=false
    fi

    AM_CONDITIONAL(WANT_ODE, $WANT_ODE)
    if $WANT_ODE; then
        AC_DEFINE([WANT_ODE], 1,
                  [Define if you are building against libode])
    fi
])
