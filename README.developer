Responsible Proto development means that every time you change
something in a release directory (/, lib/, resrc/) you should
do the following things before checking in the code:

1. Update README files and manuals, as appropriate
   In manuals, things may be marked with "\bugs{}" "\broken{}"
   or "\epxerimental{}" when they aren't working right.
2. Add regression tests to verify that your changes work
3. Keep lines to 80 characters

Doing these immediately will help prevent version skew and
accumulation of small tasks into large & painful tasks.

To act as a developer, you will need access to the subversion
repository.  Email proto-leadership@csail.mit.edu with a case for
access if you think you need it, and we'll set you up with
access to the repository at https://svn.csail.mit.edu/proto/
and add you to proto-developers@csail.mit.edu

